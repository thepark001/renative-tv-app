import React from 'react';
import ReactDOM from 'react-dom';
import App from './src/app-no-webview';

ReactDOM.render(React.createElement(App), document.getElementById('root'));
