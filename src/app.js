import React from 'react';
import {StyleSheet, View, Platform} from 'react-native';
import {WebView} from 'react-native-webview';
import Menu from 'components/menu';

const App: () => React$Node = () => {
    let player = require('./index-player.html');
    console.log('test player', player);

    /**
     * Work around to use webview
     * https://github.com/react-native-community/react-native-webview/blob/master/docs/Guide.md
     */
    return (
        <View style={{flex: 1}}>
            <WebView
                source={player}
                originWhitelist={['*', 'https://*']}
                allowFileAccess={true}
                allowUniversalAccessFromFileURLs={true}
            />
        </View>
    );
};

export default App;
