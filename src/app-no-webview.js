/*
* **SOURCES :
* WEBOS
* http://webostv.developer.lge.com/api/web-api/mediaoption-parameter/
* http://webostv.developer.lge.com/application/files/api_references/20191111_1573463601/
*
*
* */

import React, { useCallback, useEffect, useState } from 'react';
import { Icon, Button, Api, registerFocusManger, registerServiceWorker } from 'renative';
import Menu from "./components/menu";


let SRGLetterbox = null;
let srgLetterboxPlayer = null;

const styles = {
    videoPlayer: {
        alignSelf: 'center',
        width: '60vw'
    }
};

function initializePlayer() {
    console.log('initializePlayer --');
    const optionsParam = {
        debug: true,
        ilHost: 'il.srgssr.ch',
        // ilHost: 'play-mmf.herokuapp.com',
        fillMode: true,
        playerFocus: true,
        hdMode: undefined,
        language: 'en',
        controls: false,
        controlBar: false,
        subdivisionsContainer: false,
    };
    SRGLetterbox = window.SRGLetterbox;

    srgLetterboxPlayer = new SRGLetterbox(optionsParam);
    srgLetterboxPlayer.playerOptions.controls = false;
    srgLetterboxPlayer.initializeOnElement('letterboxWebPlayer');
    srgLetterboxPlayer.prepareToPlayURN('urn:swi:video:44927366'); // swi mp4
    // srgLetterboxPlayer.prepareToPlayURN('urn:rts:video:_andreteststream1'); // hls
    // srgLetterboxPlayer.prepareToPlayURN('urn:rts:video:3608506'); // rts live


    improveLayoutIfNeeded();
}

function improveLayoutIfNeeded() {
    if (srgLetterboxPlayer) {
        srgLetterboxPlayer.player.ready(() => {
            const videoEl = document.querySelector('.vjs-srgssr-skin');
            if (videoEl && videoEl.classList.contains('vjs-fill')) {
                videoEl.classList.remove('vjs-fill');
                videoEl.classList.add('vjs-fluid');
            }
        })
    }
}

function playURN(urn) {
    if (srgLetterboxPlayer.getUrn() === urn){
        srgLetterboxPlayer.paused() ? srgLetterboxPlayer.play() : srgLetterboxPlayer.pause();
    }else {
        srgLetterboxPlayer.prepareToPlayURN(urn);
        srgLetterboxPlayer.play();
    }
}

/*
* Work around to remove renative script for focused element
**/
function removeFocusedScript() {
    const focusedStyle = document.createElement('style');
    focusedStyle.rel = 'stylesheet';
    focusedStyle.type = 'text/css';
    focusedStyle.appendChild(
        document.createTextNode(`:focus {
          border: none !important;
    }
  `),
    );
    document.head.appendChild(focusedStyle);
}

const App = () => {
    console.log('App --');
    removeFocusedScript();
    const baseUrl = '//letterbox-web-2.s3.amazonaws.com/stage';
    const script = document.createElement('script');

    script.async = true;
    script.src = `${baseUrl}/letterbox.js`;
    script.type = 'text/javascript';

    script.onload = initializePlayer;

    document.body.appendChild(script);

    let link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = `${baseUrl}/letterbox.css`;
    link.media = 'all';

    document.head.appendChild(link);

    registerFocusManger({ focused: 'border: 5px solid #62DBFB; border-radius:5px;' });
    registerServiceWorker();

    return (
        <React.Fragment>
            <div id={'letterboxWebPlayer'} style={styles.videoPlayer}/>
            <Menu playURN={playURN}/>
        </React.Fragment>


    );
};

export default App;





