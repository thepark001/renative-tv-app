import React, { useEffect, useState } from 'react';
import { Text, Image, View, TouchableOpacity, StyleSheet, ScrollView } from 'react-native';
import Icon from "renative/src/Icon";
import Api from "renative/src/Api";

const hasFocus = Api.formFactor === 'tv' && Api.platform !== 'tvos';
const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        border: 'none',
        margin: '10px',
    },
    blurState: {
        opacity: .7,
    },
    focusState: {
        opacity: 1,
        border: 'none',
        outline:'none !important',
    },
    thumb: {
        height: '84px',
        minWidth: '150px',

    }
});

const Button = (props) => {
    const [state, setState] = useState({currentStyle: styles.blurState});
    const { iconName, iconFont, iconColor, className, testID } = props;

    return(
        <TouchableOpacity
            testID={testID}
            className={className}
            style={[styles.button, state.currentStyle]}
            onPress={() => {
                props.onPress();
            }}
            onFocus={() => {
                if (hasFocus) setState({ currentStyle: styles.focusState });
            }}
            onBlur={() => {
                if (hasFocus) setState({ currentStyle: styles.blurState });
            }}
        >

            <Image
                style={styles.thumb}
                source={`${props.imageUrl}/scale/width/448`}
            />
        </TouchableOpacity>
    )
};

export default Button;
