import React, { useEffect, useState } from 'react';
// import { Button } from 'renative';
import Button from './button';

const styles = {
    button: {
        marginTop: 10,
        minWidth: 100,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    buttonContainer: {
        width: '100vw',
        display: 'flex',
        justifyContent: 'center',
        flexWrap: 'wrap',

    },
};

const Menu = (props) => {
    const [medias, setMedias] = useState({});
    const [render, setRender ] = useState(false);
    const mediaListUrl = 'http://il.srgssr.ch/integrationlayer/2.0/swi/mediaList/video/mostClicked';

    useEffect(() => {
        fetch(mediaListUrl)
            .then(res => res.json())
            .then((res) => {
                setMedias(res);
                setRender(true);
            })
    }, [render]);

    function getFetchElements() {
        return (
            medias.mediaList.map((data, index) => {
                return  <Button
                    style={styles.button}
                    title={data.title}
                    className='focusable'
                    imageUrl={data.imageUrl}
                    onPress={() => {
                        console.log('button', data);
                        props.playURN(data.urn);
                    }}
                />
            })
        )
    }

    return(
        <div style={styles.buttonContainer}>
            {render ? getFetchElements()
                    : console.log('no mediaList')
            }
        </div>
    )
};

export default Menu;
